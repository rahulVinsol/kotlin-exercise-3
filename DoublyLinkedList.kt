/**
 * A Doubly Linked List that stores Nodes containing data in a Linear Order
 *
 * @param T type of data the Node will contain
 * @property head Property that stores the first Node in List
 * @property tail Property that stores the last Node in List
 * @property size Property that stores size of LinkedList
 * @constructor Creates an Empty DoublyLinkedList
 */
class DoublyLinkedList<T> {
    private var head: Node<T>? = null
    private var tail: Node<T>? = null
    var size: Int = 0
        private set;

    /**
     * class that defines Individual Node in Linked List
     *
     * @param T the type of data in a Node
     * @property data the data that this Node contains
     * @property prev the Node that exists before current Node in list
     * @property next the Node that exists after current Node in list
     * @constructor Creates a new disconnected Node with data
     */
    class Node<T>(var data: T) {
        internal var prev: Node<T>? = null
        internal var next: Node<T>? = null
    }

    /**
     * Prints the LinkedList from Start to End
     *
     * @return nothing
     */
    fun forwardTraverse() {
        println();
        if (head == null) {
            println("Nothing in Linked List")
            return;
        }
        println("Forward Traversing is as Follows: ");
        var node: Node<T>? = head!!
        while (node != null) {
            print("${node.data}")
            node = node.next;
            if (node != null) {
                print(" -> ")
            }
        }
    }

    /**
     * Prints the LinkedList from End to Start
     *
     * @return nothing
     */
    fun reverseTraverse() {
        println();
        if (tail == null) {
            println("Nothing in Linked List")
            return;
        }
        println("Reverse Traversing is as Follows: ");
        var node: Node<T>? = tail!!
        while (node != null) {
            print("${node.data}")
            node = node.prev;
            if (node != null) {
                print(" -> ")
            }
        }
    }

    /**
     * Adds a new Node containing [data] to the beginning of LinkedList
     *
     * @return new size of LinkedList
     */
    fun insertBeginning(data: T): Int {
        val newNode = Node<T>(data);
        if (head == null && tail == null) {
            head = newNode;
            tail = newNode;
        } else {
            newNode.next = head;
            head!!.prev = newNode;
            head = newNode;
        }
        return ++size;
    }

    /**
     * Adds a new Node containing [data] at the End of LinkedList
     *
     * @return new size of LinkedList
     */
    fun insertEnd(data: T): Int {
        val newNode = Node<T>(data);
        if (head == null && tail == null) {
            head = newNode;
            tail = newNode;
        } else {
            newNode.prev = tail;
            tail!!.next = newNode;
            tail = newNode;
        }
        return ++size;
    }

    /**
     * Adds a new Node containing [newData] before any other Node containing [data]
     *
     * @return new size of LinkedList
     */
    fun insertBefore(data: T, newData: T): Int {
        val newNode = Node<T>(newData);
        head ?: return size;
        val node = searchData(data);
        node ?: return size;
        if (node.prev == null) {
            insertBeginning(newData);
            return size;
        } else {
            newNode.prev = node.prev;
            node.prev!!.next = newNode;
            node.prev = newNode;
            newNode.next = node;
        }
        return ++size;
    }

    /**
     * Adds a new Node containing [newData] after any other Node containing [data]
     *
     * @return new size of LinkedList
     */
    fun insertAfter(data: T, newData: T): Int {
        val newNode = Node<T>(newData);
        head ?: return size;
        val node = searchData(data);
        node ?: return size;
        if (node.next == null) {
            insertEnd(newData);
            return size;
        } else {
            newNode.next = node.next;
            node.next!!.prev = newNode;
            node.next = newNode;
            newNode.prev = node;
        }
        return ++size;
    }

    /**
     * Searches for Node containing [data] in the LinkedList
     *
     * @return the Node containing [data]
     */
    private fun searchData(data: T): Node<T>? {
        var node = head;
        while (node != null && node.data != data) {
            node = node.next;
        }
        return node;
    }

    /**
     * Removes the first Node containing [data] from the LinkedList in forward direction
     *
     * @return new Size of LinkedList
     */
    fun removeNode(data: T): Int {
        head ?: return size;
        val node = searchData(data);
        node ?: return size;
        when {
            head == tail -> {
                head = null;
                tail = null;
            }
            head == node -> {
                head = head!!.next;
                head!!.prev = null;
                node.next = null;
            }
            tail == node -> {
                tail = tail!!.prev;
                tail!!.next = null;
                node.prev = null;
            }
            else -> {
                val nodePrev = node.prev;
                val nodeNext = node.next;
                node.next = null;
                node.prev = null;
                nodePrev!!.next = nodeNext;
                nodeNext!!.prev = nodePrev;
            }
        }
        return --size;
    }
}

fun main(args: Array<String>) {
    val dll = DoublyLinkedList<Int>()
    dll.forwardTraverse()
    println("Inserting Data:- ")
    println(dll.insertBeginning(2))
    println(dll.insertAfter(2, 3))
    println(dll.insertEnd(4))
    println(dll.insertBeginning(1))
    dll.reverseTraverse()
    dll.forwardTraverse()
    println("\nRemoving Data:- ")
    println(dll.removeNode(1))
    println(dll.removeNode(2))
    println(dll.removeNode(3))
    println(dll.removeNode(4))
    dll.forwardTraverse()
}